'use strict'
var app = require('./app');

var Global = require('./global');
let redis = require('redis');

const canal = 'nuevapublicacion';

let redisClient = redis.createClient(Global.Global.urlredis);
redisClient.subscribe(canal);

var port = process.env.PORT || 3005;

app.listen(port, function () {
    console.log('Servidor de api rest escuchando en el puerto: ' + port);
});

console.log("Server Redis: "+Global.Global.urlredis);
redisClient.get("descarga",function (err, reply) {
    console.log("Valor prueba: "+reply);
});